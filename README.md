# Webpage

## How to test

Get jekyll, it is available in Ubuntu.

```bash
sudo apt install jekyll
```

Then run this command in the repo:

```bash
jekyll s
```

If your `jekyll` is new enough it has inbuilt livereload support.

```bash
jekyll s --livereload
```
